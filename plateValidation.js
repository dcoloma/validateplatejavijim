function isValidPlate(plate) { // eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  gtag('event', 'click', { // eslint-disable-line
    'event_category': 'calculate',
    'event_label':plate.match(re)
  });
  if(plate.match(re) != null) {
    return true;
  } else {
    return false;
  }
}